#include <stdio.h>

void input(int m,int n,int a[m][n])
{
    int i,j;
    for(i=0;i<m;i++)
    {
        printf("Enter marks of Student %d\n",i+1);
        for(j=0;j<n;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
}

void compute(int m,int n,int hoc[],int hos[],int sub_hos[],int a[m][n])
{
    int i,j;
    for(j=0;j<n;j++)
    {
        i=0;
        hoc[j]=a[i][j];
        for(;i<m;i++)
        {
            if(a[i][j]>hoc[j])
            {
                hoc[j]=a[i][j];
            }
        }
    }
    
    for(i=0;i<m;i++)
    {
        j=0;
        sub_hos[i]=j+1;
        hos[i]=a[i][j];
        for(;j<n;j++)
        {
            if(a[i][j]>hos[i])
            {
                hos[i]=a[i][j];
                sub_hos[i]=j+1;
            }
        }
    }
}

void output(int m,int n,int hoc[],int hos[],int sub_hos[])
{
    int i;
    printf("\n");
    for(i=0;i<n;i++)
    {
        printf("Highest marks in Course %d : %d\n",i+1,hoc[i]);
    }
    printf("\n");
    for(i=0;i<m;i++)
    {
        printf("Highest marks of Student %d is %d in Course %d\n",i+1,hos[i],sub_hos[i]);
    }
}

int main()
{
    int a[5][3],hoc[3],hos[5],sub_hos[5];
    input(5,3,a);
    compute(5,3,hoc,hos,sub_hos,a);
    output(5,3,hoc,hos,sub_hos);
    return 0;
}