#include <stdio.h>

void size(int *n)
{
    printf("Enter size of the array\n");
    scanf("%d",n);
}

void input(int n,int a[],int *s)
{
    int i;
    printf("Enter elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the element to be searched\n");
    scanf("%d",s);
}

void compute(int n,int a[],int s,int *p)
{
    int i;
    for(i=0;i<n;i++)
    {
        if(s==a[i])
        {
            *p=i;
            break;
        }
    }
}

void output(int s,int p)
{
    if(p!=-1)
        printf("The element %d is present in the array at index position %d\n",s,p);
    else
        printf("The element %d could not be found in the array\n",s);
}

int main()
{
    int n,s,p=-1;
    size(&n);
    int a[n];
    input(n,a,&s);
    compute(n,a,s,&p);
    output(s,p);
    return 0;
}