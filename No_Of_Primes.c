#include <stdio.h>
int size()
{
    int n;
    printf("Enter N\n");
    scanf("%d",&n);
    return n;
}

void input(int n,int a[])
{
    int i;
    printf("Enter the numbers\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}

int compute(int n,int a[])
{
    int i,c,p=0,j;
    for(i=0;i<n;i++)
    {
        c=0;
        for(j=1;j<a[i];j++)
        {
            if(a[i]%j==0)
                c++;
        }
        if(c==2)
            p++;
    }
    return p;
}

void output(int p,int c)
{
    printf("Number of primes : %d\nNumber of composites : %d\n",p,c);
}

int main()
{
    int n,p;
    n=size();
    int a[n];
    input(n,a);
    p=compute(n,a);
    output(p,n-p);
    return 0;
}