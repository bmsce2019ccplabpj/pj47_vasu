#include <stdio.h>

void size(int *s)
{
    printf("Enter size of array\n");
    scanf("%d",s);
}

void input(int s,int a[s])
{
    int i;
    printf("Enter the elements of array\n");
    for(i=0;i<s;i++)
        scanf("%d",&a[i]);
}

void swap(int *a,int *b)
{
    int c;
    c=*a;
    *a=*b;
    *b=c;
}

void compute(int s,int a[s])
{
    int i=1,t=1,j=s-1,m1=-1;
    while(j>=i)
    {
        if(a[j]<=a[0])
        {
            m1=1;
            if(j==i)
                break;
            i=t;
            while(i<j)
            {
                if(a[i]>a[0])
                {
                    swap(&a[i],&a[j]);
                    t=i+1;
                    break;
                }
                i++;
            }
        }
        j--;
    }
    if(m1==1)
        swap(&a[0],&a[i]);
}

void output(int s,int a[s])
{
    int i;
    printf("\n");
    for(i=0;i<s;i++)
    {
        printf("%d ",a[i]);
    }
}

int main()
{
    int s;
    size(&s);
    int a[s];
    input(s,a);
    compute(s,a);
    output(s,a);
    return 0;
}