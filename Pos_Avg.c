#include <stdio.h>
int input()
{
    int n;
    printf("Enter N\n");
    scanf("%d",&n);
    return n;
}

float compute(int n)
{
    int i,c=0;
    float a,s=0;
    printf("Enter total N positive and negative numbers\n");
    for(i=0;i<n;i++)
        {
            scanf("%f",&a);
            if(a>=0)
            {
                s=s+a;
                c++;
            }
        }
    return (s/c);
}

void output(float a)
{
    printf("Average of all positive numbers : %.2f\n",a);
}

int main()
{
    int n;
    n=input();
    float a=compute(n);
    output(a);
    return 0;
}