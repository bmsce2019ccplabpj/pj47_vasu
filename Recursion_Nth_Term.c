#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int find_nth_term(int n, int a, int b, int c)
{
    int d = a+b+c;
    a=b;
    b=c;
    c=d;
    n--;
    if(n>3)
    find_nth_term(n,a,b,c);
    else
    return d;
}

int main()
{
    int n, a, b, c;
    printf("Enter N\n");
    scanf("%d",&n);
    printf("Enter first term\n");
    scanf("%d", &a);
    printf("Enter second term\n");
    scanf("%d",&b);
    printf("Enter third term\n");
    scanf("%d",&c);
    int ans = find_nth_term(n, a, b, c);
 
    printf("Nth term = %d", ans); 
    return 0;
}