#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
    int i,c=0;
    for(i=1;i<=n;i++)
    {
        if(n%i==0)
            c++;
    }
    return c;
}

void output(int n,int c)
{
    if(c==2)
        printf("%d is a prime number\n",n);
    else
        printf("%d is a composite number\n",n);
}

int main()
{
    int n,c;
    n=input();
    c=compute(n);
    output(n,c);
    return 0;
}