#include <stdio.h>

void size(int *n)
{
    printf("Enter the size of array\n");
    scanf("%d",n);
}

void input(int *s,int n,int a[])
{
    int i;
    printf("Enter elements of array\n");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("Enter search element\n");
    scanf("%d",s);
}

void bubble_sort(int n,int a[])
{
    int i,j,c;
    for(i=0;i<n-1;i++)
    {
        for(j=0;j<n-i-1;j++)
        {
            if(a[j]>a[j+1])
            {
                c=a[j];
                a[j]=a[j+1];
                a[j+1]=c;
            }
        }
    }
}

void binary_search(int s,int n,int a[],int *f)
{
    int l=0,h=n-1,m;
    *f=0;
    while(l<=h)
    {
        m=(l+h)/2;
        if(s==a[m])
        {
            *f=1;
            break;
        }
        else if(s<a[m])
        {
            h=m-1;
        }
        else
        {
            l=m+1;
        }
    }
}

void output(int s,int n,int a[],int f)
{
    int i;
    printf("Sorted array in ascending order : ");
    for(i=0;i<n;i++)
        printf("%d ",a[i]);
    if(f==1)
        printf("\nSearch Successful\n%d is present in the array\n",s);
    else
        printf("\nSearch Unsuccessful\n%d could not be found in the array\n",s);
}

void main()
{
    int n,s,f;
    size(&n);
    int a[n];
    input(&s,n,a);
    bubble_sort(n,a);
    binary_search(s,n,a,&f);
    output(s,n,a,f);
}