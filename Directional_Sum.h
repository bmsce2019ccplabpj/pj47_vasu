#include <stdio.h>
#include <ctype.h>

void input(int *n,char *c,int *l1)
{
    printf("Enter the number\n");
    scanf("%d",n);
    printf("Enter the direction of sum\n");
    scanf(" %c",c);
    printf("Enter number of digits\n");
    scanf("%d",l1);
}


int sumr(int n,int l1)
{
    int x=0,i;
    for(i=1;i<=l1;i++)
    {
        x=x+(n%10);
        n=n/10;
    }
    return x;
}

int suml(int n,int l1)
{
    int i,x=0,y=0,l2=0;
    while(n!=0)
    {
        x=(x*10)+(n%10);
        n=n/10;
    }
    for(i=1;i<=l1;i++)
    {
        y=y+(x%10);
        x=x/10;
    }
    return y;
}

int compute(int n,char c,int l1)
{
    int sum;
    c=tolower(c);
    if(c=='r')
        sum=sumr(n,l1);
    else if(c=='l')
        sum=suml(n,l1);
    return sum;
}

void output(int sum)
{
    printf("Sum = %d\n",sum);
}

int main()
{
    int n,l1,sum;
    char c;
    input(&n,&c,&l1);
    sum = compute(n,c,l1);
    output(sum);
    return 0;
}