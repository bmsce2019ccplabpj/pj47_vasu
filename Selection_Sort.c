#include <stdio.h>

void size(int *n)
{
    printf("Enter size of the array\n");
    scanf("%d",n);
}

void input(int n,int a[n])
{
    int i;
    printf("Enter elements of array\n");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
}

void minimum(int i,int n,int a[n],int *min_p)
{
    int j;
    *min_p=i;
    for(j=i;j<n;j++)
    {
        if(a[j]<a[*min_p])
            *min_p= j;
    }
}

void swap(int *x,int *y)
{
    int c;
    c=*x;
    *x=*y;
    *y=c;
}

void selection_sort(int n,int a[n])
{
    int i,min_p;
    for(i=0;i<n;i++)
    {
        minimum(i,n,a,&min_p);
        swap(&a[i],&a[min_p]);
    }
}

void output(int n,int a[n])
{
    int i;
    printf("Sorted array : ");
    for(i=0;i<n;i++)
        printf("%d ",a[i]);
}

int main()
{
    int n;
    size(&n);
    int a[n];
    input(n,a);
    selection_sort(n,a);
    output(n,a);
}