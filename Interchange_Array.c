#include <stdio.h>

void size(int *n)
{
    printf("Enter size of the array\n");
    scanf("%d",n);
}

void input(int n,int a[])
{
    int i;
    printf("Enter elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}

void compute(int n,int a[],int *min,int *min_pos,int *max,int *max_pos)
{
    int i,c;
    *min=a[0],*max=a[0];
    for(i=0;i<n;i++)
    {
        if(a[i]<=*min)
        {
            *min=a[i];
            *min_pos=i;
        }
        if(a[i]>=*max)
        {
            *max=a[i];
            *max_pos=i;
        }
    }
    
    c=a[*min_pos];
    a[*min_pos]=a[*max_pos];
    a[*max_pos]=c;
}

void output1(int n,int a[])
{
    int i;
    printf("Array before interchange : ");
    for(i=0;i<n;i++)
    {
        printf("%d ",a[i]);
    }
}

void output2(int n,int a[],int min,int min_pos,int max,int max_pos)
{
    int i;
    printf("\nInitial index position of smallest number : %d\nInitial index position of largest number : %d\nSmallest number : %d\nLargest number : %d\n",min_pos,max_pos,min,max);
    printf("Array after interchange : ");
    for(i=0;i<n;i++)
    {
        printf("%d ",a[i]);
    }
}

int main()
{
    int n,min,min_pos,max,max_pos;
    size(&n);
    
    int a[n];
    input(n,a);
    output1(n,a);
    
    compute(n,a,&min,&min_pos,&max,&max_pos);
    
    output2(n,a,min,min_pos,max,max_pos);
    
    return 0;
}