#include <stdio.h>

struct time
{
    int hr,mn;
};

int number(int *n)
{
    printf("Enter the number of times\n");
    scanf("%d",n);
}

void input(int n,struct time t[n])
{
    int i;
    printf("Enter the times hours and minutes\n");
    for(i=0;i<n;i++)
        scanf("%d%d",&t[i].hr,&t[i].mn);
}

void compute(int n,struct time t[n],int *z)
{
    int i;
    for(i=0;i<n;i++)
    {
        *z = (t[i].hr*60) + t[i].mn;
        z++;
    }
}

void output(int n,int z[])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("%d\n",z[i]);
    }
}

int main()
{
    int n;
    number(&n);
    struct time t[n];
    int z[n];
    input(n,t);
    compute(n,t,z);
    output(n,z);
    return 0;
}