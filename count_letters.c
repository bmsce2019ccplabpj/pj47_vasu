#include <stdio.h>

void count(int *uc,int *lc,int *nc)
{
    char c;
    printf("Enter characters\n");
    scanf("%c",&c);
    while(c!='*')
    {
        if(c>='A' && c<='Z')
            *uc+=1;
        else if(c>='a' && c<='z')
            *lc+=1;
        else if(c>='0' && c<='9')
            *nc+=1;
        scanf("%c",&c);
    }
}

void output(int uc,int lc,int nc)
{
    printf("Number of uppercase : %d\nNumber of lowercase : %d\nNumber of numbers   : %d\n",uc,lc,nc);
}

void main()
{
    int uc=0,lc=0,nc=0;
    count(&uc,&lc,&nc);
    output(uc,lc,nc);
}