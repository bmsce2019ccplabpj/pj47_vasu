#include <stdio.h>
#include <ctype.h>

int reverse(int n,int a)
{
    int i;
    a = a*10 + n%10;
    n = n/10;
    if(n!=0)
    reverse(n,a);
    else
    return a;
}

int sum(int n,int l,int x,int i)
{
    if(i<=l)
    {
        x = x + (n%10);
        n = n/10;
        i++;
        sum(n,l,x,i);
    }
    else
    return x;
}

int main()
{
    int n,l,x,n1,i;
    char c;
    scanf("%d%d %c",&n,&l,&c);
    c = tolower(c);
    n1=n;
    if(c=='l')
    n1 = reverse(n,0);
    x = sum(n1,l,0,1);
    printf("%d",x);
}