#include<stdio.h>

void disp(char a[3][3])
{
     int i,j;
     printf("\n");
     for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            printf("%c ",a[i][j]);
            printf("\n");
        }
}

int check(char a[3][3])
{
    if( (a[0][0]!='-' && a[0][0]==a[1][1] && a[1][1]==a[2][2])
    ||  (a[0][2]!='-' && a[0][2]==a[1][1] && a[1][1]==a[2][0])
    ||  (a[0][0]!='-' && a[0][0]==a[0][1] && a[0][1]==a[0][2])
    ||  (a[1][0]!='-' && a[1][0]==a[1][1] && a[1][1]==a[1][2])
    ||  (a[2][0]!='-' && a[2][0]==a[2][1] && a[2][1]==a[2][2])
    ||  (a[0][0]!='-' && a[0][0]==a[1][0] && a[1][0]==a[2][0])
    ||  (a[0][1]!='-' && a[0][1]==a[1][1] && a[1][1]==a[2][1])
    ||  (a[0][2]!='-' && a[0][2]==a[1][2] && a[1][2]==a[2][2])
      )
    return 1;
    else 
    return 0;
}

int main()
{
    int n=1,i,j,x,y;
    char a[3][3];
    for(i=0;i<3;i++)
    for(j=0;j<3;j++)
    a[i][j] = '-';
    
    printf("Co-ordinates : \n");
    for(i=1;i<=3;i++)
    {
        for(j=1;j<=3;j++)
        printf("%d %d   ",i,j);
        printf("\n");
    }
    
    printf("\nPlayers should enter the co-ordinates seperated by a space\n");
    while(n<=9)
    {
        printf("\nPlayer X\n");
        scanf("%d%d",&i,&j);
        a[i-1][j-1] = 'X';
        disp(a);
        x = check(a);
        if(x==1 || n==9)
        break;
        n++;
        printf("\nPlayer O\n");
        scanf("%d%d",&i,&j);
        a[i-1][j-1] = 'O';
        disp(a);
        y = check(a);
        if(y==1)
        break;
        n++;
    }
    printf("\nRESULT : ");
    disp(a);
    if(x==1)
    printf("PLAYER X WINS\n");
    else if(y==1)
    printf("PLAYER O WINS\n");
    else
    printf("CAT'S GAME : NO ONE WINS\n");
}