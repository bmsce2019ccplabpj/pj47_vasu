#include <stdio.h>
#include <math.h>

struct circle
{
    float rad,x,y;
};
struct circle_pair
{
    struct circle c1;
    struct circle c2;
};

void number(int *n)
{
    printf("Enter number of pairs of circles to be analyzed\n");
    scanf("%d",n);
}

void input(int n,struct circle_pair cp[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("For Pair %d :\n",(i+1));
        printf("Enter radius,x co-ordinate and y co-ordinate of circle 1 : ");
        scanf("%f%f%f",&cp[i].c1.rad,&cp[i].c1.x,&cp[i].c1.y);
        printf("Enter radius,x co-ordinate and y co-ordinate of circle 2 : ");
        scanf("%f%f%f",&cp[i].c2.rad,&cp[i].c2.x,&cp[i].c2.y);
    }
    
}

void distance(int n,struct circle_pair cp[n],float dis[])
{
    int i;
    for(i=0;i<n;i++)
    {   
        dis[i]=sqrt((cp[i].c1.x-cp[i].c2.x)*(cp[i].c1.x-cp[i].c2.x) + (cp[i].c1.y-cp[i].c2.y)*(cp[i].c1.y-cp[i].c2.y));
    }
}

void check(int n,struct circle_pair cp[n],int res[])
{
    float dis[n];
    int i;
    distance(n,cp,dis);
    for(i=0;i<n;i++)
    {
        if((cp[i].c1.rad+cp[i].c2.rad)>dis[i])
            res[i]=1;
        else
            res[i]=0;
    }
}

void output(int n,int res[])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("Pair %d circles ",(i+1));
        if(res[i]==1)
            printf("intersect\n");
        else
            printf("do not intersect\n");
    }
}

int main()
{
    int n;
    number(&n);
    int res[n];
    struct circle_pair cp[n];
    input(n,cp);
    check(n,cp,res);
    output(n,res);
    return 0;
}