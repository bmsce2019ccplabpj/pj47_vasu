#include <stdio.h>

void input(int *n,int a[],int b[])
{
    printf("Enter the number of fractions\n");
    scanf("%d",n);
    printf("Enter the fractions as n/d\n");
    for(int i=0;i<*n;i++)
    scanf("%d/%d",&a[i],&b[i]);
}

void compute(int n,int a[],int b[],int *s1,int *s2)
{
    for(int i=0;i<n;i++)
    *s2 = (*s2)*b[i];
    for(int i=0;i<n;i++)
    *s1 = *s1 + (a[i]*(*s2)/b[i]);
    for(int i=(*s2>=*s1)?*s2:*s1;i>=1;i--)
    {
        if(*s2%i==0 && *s1%i==0)
            {
              *s2=*s2/i;
              *s1=*s1/i;
            }
    }
}

void output(int s1,int s2)
{
    printf("\nSum = %d/%d\n",s1,s2);
}
int main()
{
    int n,s2=1,s1=0,a[n],b[n];
    input(&n,a,b);
    compute(n,a,b,&s1,&s2);
    output(s1,s2);
}