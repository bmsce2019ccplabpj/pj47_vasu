#include <stdio.h>

struct fractions
{
    int n,d;
};

int size()
{
    int s;
    printf("Enter number of fractions\n");
    scanf("%d",&s);
    return s;
}
void input(int s,struct fractions a[])
{
    printf("Enter the fractions as n/d\n");
    for(int i=0;i<s;i++)
    scanf("%d/%d",&a[i].n,&a[i].d);
}

void compute(int s,struct fractions a[])
{
    int i;
    a[s].d=1;
    a[s].n=0;
    for(i=0;i<s;i++)
        a[s].d = (a[s].d)*(a[i].d);
    for(i=0;i<s;i++)
        (a[s].n) = (a[s].n) + ((a[i].n)*(a[s].d)/(a[i].d));
    i=((a[s].d)>=(a[s].n))?(a[s].d):(a[s].n);
    for(;i>=1;i--)
    {
        if(((a[s].d)%i==0) && ((a[s].n)%i==0))
            {
              (a[s].d)=(a[s].d)/i;
              (a[s].n)=(a[s].n)/i;
            }
    }
}

void output(int s,struct fractions a[])
{
    printf("\nSum = %d/%d\n",a[s].n,a[s].d);
}
int main()
{
    int s = size();
    struct fractions a[s];
    input(s,a);
    compute(s,a);
    output(s,a);
    return 0;
}