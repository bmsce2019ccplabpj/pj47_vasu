#include <stdio.h>
int input();
int compute(int,int,int);
void output(int);

int main()
{
    output(compute(input(),input(),input()));
    return 0;
}   
int input()
{
    int x;
    printf("Enter number\n");
    scanf("%d",&x);
    return x;
}
int compute(int a,int b,int c)
{
    int max;
    if(a>=b && a>=c)
        max = a;
    else if(b>=c)
        max = b;
    else
        max = c;
    return max;
}
void output(int max)
{
    printf("\n%d is the largest number\n",max);
}