#include <stdio.h>

void input(int *n1,int *d1,int *n2,int *d2)
{
    printf("Enter both fractions as n/d \n");
    scanf("%d/%d %d/%d",n1,d1,n2,d2);
}

void compute(int n1,int d1,int n2,int d2,int *s1,int *s2)
{
    int i;
    *s1=((n1*d2)+(n2*d1));
    *s2=d1*d2;
    for(i=(*s2>=*s1)?*s2:*s1;i>=1;i--)
    {
        if(*s2%i==0 && *s1%i==0)
            {
              *s2=*s2/i;
              *s1=*s1/i;
            }
    }
}

void output(int s1,int s2)
{
    printf("Sum = %d/%d\n",s1,s2);
}

int main()
{
    int n1,d1,n2,d2,s1,s2;
    input(&n1,&d1,&n2,&d2);
    compute(n1,d1,n2,d2,&s1,&s2);
    output(s1,s2);
    return 0;   
}