#include <stdio.h>
#include <string.h>

struct details
{
    char name[25];
    int id;
    int score;
};

void size(int *n)
{
    printf("Enter number of students\n");
    scanf("%d",n);
}

void fname(char fl[])
{
    printf("Enter file name\n");
    scanf("%s",fl);
}

void st_input(int n,struct details stu1[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("\nEnter details of student %d\n",(i+1));
        printf("Enter student name  : ");
        scanf(" %s",stu1[i].name);
        printf("Enter student id   : ");
        scanf("%d",&stu1[i].id);
        printf("Enter student score : ");
        scanf("%d",&stu1[i].score);
    }
}

void st_output(int n,struct details stu2[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("\nStudent %d\n",(i+1));
        printf("Name  : %s\nID    : %d\nScore : %d\n",stu2[i].name,stu2[i].id,stu2[i].score);
    }
}

void f_write(FILE *fptr,int n,struct details stu1[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        fprintf(fptr," %s\n",stu1[i].name);
    	fprintf(fptr," %d\n",stu1[i].id);
    	fprintf(fptr," %d\n",stu1[i].score);
    }
}

void f_read(FILE *fptr,int n,struct details stu2[n])
{
    int i;
	for(i=0;i<n;i++)
    {
        fscanf(fptr," %s",stu2[i].name);
    	fscanf(fptr," %d",&stu2[i].id);
    	fscanf(fptr," %d",&stu2[i].score);
    }
}

void search_in(char sr[])
{
    printf("\nEnter name to be searched\n");
    scanf("%s",sr);
}

void search_out(int n,struct details stu2[n],char sr[])
{
    int i,c;
    for(i=0;i<n;i++)
    {
        if(strcmp(stu2[i].name,sr)==0)
        {
            printf("\nStudent %d\n",(i+1));
            printf("Name  : %s\nID    : %d\nScore : %d\n",stu2[i].name,stu2[i].id,stu2[i].score);
            c=1;
        }
    }
    if(c==0)
        printf("No data found\n");
}


int main()
{
	FILE *fptr;
	int n;
	char fl[25],sr[25];
	size(&n);
	fname(fl);
	struct details stu1[n],stu2[n];
	st_input(n,stu1);
	
	fptr = fopen(&fl,"w");
	f_write(fptr,n,stu1);
	fclose(fptr);
	
	fptr = fopen(&fl,"r");
	f_read(fptr,n,stu2);
	fclose(fptr);
	
	st_output(n,stu2);
	
	search_in(sr);
	search_out(n,stu2,sr);
	
	return 0;
}