#include <stdio.h>
void convert(float);
void output(float);

void input()
{
	float f;
	printf("Enter the temperature in Degree Fahrenheit\n");
	scanf("%f",&f);
	convert(f);
}
void convert(float f)
{
	float c =(5.0/9.0)*(f-32);
    output(c);
}
void output(float c)
{
    printf("Temperature in Degree Celsius = %.2f\n",c);
}
int main()
{
    input();
    return 0;
}