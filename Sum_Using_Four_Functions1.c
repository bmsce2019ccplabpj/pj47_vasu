#include <stdio.h>
void sum(int,int);
void output(int);

void input()
{
    int a,b;
    printf("Enter two numbers\n");
    scanf("%d%d",&a,&b);
    sum(a,b);
}
void sum(int a,int b)
{
    int s = a + b;
    output(s);
}
void output(int s)
{
    printf("Sum = %d\n",s);
}
int main()
{
    input();
    return 0;
}