#include<stdio.h>
#include<math.h>

struct complex{
    float re,im;
};

void input(float *a,float *b,float *c)
{
    printf("Enter the coefficients of the quadratic equation\n");
    scanf("%f%f%f",a,b,c);
}

void roots(float a,float b,float c,int *r,float *x1,float *x2,struct complex *y)
{
    float d = (b*b)-(4*a*c);
    if(d>0)
    {
        *r = 2;
        *x1 = (-b + sqrt(d))/(2*a);
        *x2 = (-b - sqrt(d))/(2*a);
    }
    else if(d==0)
    {
        *r = 1;
        *x1 = (-b)/(2*a);
    }
    else
    {
        d=d*(-1);
        y->re=(-b)/(2*a);
        y->im=(sqrt(d)/(2*a));
    }
}

void output(int r,float x1,float x2,struct complex y)
{
    if(r==2)
    printf("There are two real roots :\nx1 = %.2f\nx2 = %.2f\n",x1,x2);
    else if(r==1)
    printf("Both roots are equal\nx = %.2f\n",x1);
    else
    printf("Roots are complex\nx1 = %.2f+%.2fi\nx2 = %.2f-%.2fi\n",y.re,y.im,y.re,y.im);
}

int main()
{
    float a,b,c,x1,x2;
    int r;
    struct complex y;
    input(&a,&b,&c);
    roots(a,b,c,&r,&x1,&x2,&y);
    output(r,x1,x2,y);
    return 0;
}