#include <stdio.h>

void input(char str1[],char str2[])
{
    printf("Enter two strings\n");
    gets(str1);
    gets(str2);
}

int length(char str[])
{
    int i=0;
    while(str[i]!='\0')
    i++;
    return i;
}

void concat(char str1[],char str2[])
{
    int l1,l2,i;
    l1 = length(str1);
    l2 = length(str2);
    for(i=0;i<l2;i++)
    {
        str1[l1+i] = str2[i];
    }
}

void output(char str[])
{
    int i=0;
    printf("\nConcatenated String : ");
    while(str[i]!='\0')
    {
        printf("%c",str[i]);
        i++;
    }
}

int main()
{
    char str1[1000],str2[1000];
    input(str1,str2);
    concat(str1,str2);
    output(str1);
    return 0;
}