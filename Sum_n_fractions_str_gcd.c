#include <stdio.h>

struct fraction
{
    int num,den;
};

int size()
{
    int s;
    printf("Enter the number of fractions\n");
    scanf("%d",&s);
    return s;
}

void input(int s,struct fraction a[])
{
    int i;
    printf("Enter the fractions as Numerator/Denominator\n");
    for(i=0;i<s;i++)
    scanf("%d/%d",&a[i].num,&a[i].den);
}

int gcd(int n,int d)
{
    int i;
    for(i=(n<=d?n:d);i>=1;i--)
    if(n%i==0 && d%i==0)
    break;
    return i;
}

void compute(int s,struct fraction a[],struct fraction *b)
{
    int i,g;
    b->den=1;
    for(i=0;i<s;i++)
        b->den = b->den*a[i].den;
    b->num=0;
    for(i=0;i<s;i++)
        b->num = b->num + (a[i].num*b->den/a[i].den);
    g = gcd(b->num,b->den);
    b->num=b->num/g;
    b->den=b->den/g;
}

void output(int s,struct fraction a[],struct fraction b)
{
    int i;
    printf("\n");
    for(i=0;i<s-1;i++)
        printf("%d/%d + ",a[i].num,a[i].den);
    printf("%d/%d = %d/%d\n",a[s-1].num,a[s-1].den,b.num,b.den);
}

int main()
{
    int s = size();
    struct fraction a[s];
    struct fraction b;
    input(s,a);
    compute(s,a,&b);
    output(s,a,b);
    return 0;
}