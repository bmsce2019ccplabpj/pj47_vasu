#include <stdio.h>
#include <math.h>

struct distance
{
    float x;
    float y;
};

void input(struct distance *p1,struct distance *p2)
{
    printf("Enter the coordinates x1,y1,x2,y2\n");
    scanf("%f%f%f%f",&p1->x,&p1->y,&p2->x,&p2->y);
}

float compute(struct distance p1,struct distance p2)
{
    float d;
    d = sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
    return d;
}

void output(float d)
{
    printf("Distance = %f",d);
}

int main()
{
    struct distance p1,p2;
    float d;
    input(&p1,&p2);
    d = compute(p1,p2);
    output(d);
    return 0;
}